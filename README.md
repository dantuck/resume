# Resume

Resume of Daniel Tucker, built with Webpack + Pug + Postcss.

We can create static website really easilly with Netlify. Thanks!!

![screenshot](https://gitlab.com/dantuck/resume/raw/master/screenshot.png)

# Getting started

After checking out the repo, run:

```
yarn install
yarn start
```

open localhost:3000 and you can see my resume.

# Write your resume with this template

Feel free to write your resume with this template, if you are enough odd to do so.

- `src/app.postcss` is for css
- `src/index.pug` is for template
- If you want to write your resume with sass, just add `sass-loader`

# Screenshot

To take a screenshot, just run

- `yarn screenshot`

This runs your Chrome headlessly and take an image.

# Production build

To build all assets, run:

```
yarn build
```

This does:

- Bundle files with Webpack production mode
- Take screenshot as `png` and `pdf`

# Netlify

Soon

# TODO

- [x] Add script for taking screenshot.
- [x] Create GitHub page or something to render this as actual web page. → netlify
- [x] Add script for build. Generate multilingual PDF to `/dist`.
- [ ] Page too big to fit A4. Time to divide.
- [ ] Use Vue.js for template rendering. This is for HMR and scoped CSS and internationalization.
- [ ] Add ESLint for stability.
- [ ] Add Flow for type-safe and null-safe.
- [ ] `screenshot` command for all languages.

# Contribution

Thanks in advance.